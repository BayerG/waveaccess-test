from pathlib import Path

import uvicorn
from fastapi import FastAPI, Request
from pandas import read_csv
from starlette.responses import JSONResponse
from starlette.status import HTTP_400_BAD_REQUEST

DATASET_PATH = Path('datasets/salary_survey.csv')
HOST = '0.0.0.0'
PORT = 8002

app = FastAPI()

columns = ['timestamp', 'employment', 'company', 'company_size', 'country', 'city', 'industry', 'type',
           'industry_years', 'current_years', 'job_title', 'job_ladder', 'job_level', 'required_hours', 'actual_hours',
           'education', 'salary', 'bonus', 'stock', 'insurance', 'vacation', 'happy', 'resign', 'thoughts', 'gender',
           'skills', 'bootcamp']
df_original = read_csv(DATASET_PATH, names=columns, skiprows=2).infer_objects()


@app.get('/compensation_data')
def get_data(request: Request):
    try:
        df = df_original.copy()
        sort_by = None
        for key, value in request.query_params.items():
            if value.isnumeric():
                value = float(value)
            if key != 'sort':
                df = df[df[key] == value]
            else:
                sort_by = value
        if sort_by is not None:
            df.sort_values(by=sort_by, inplace=True)
        return JSONResponse(df.fillna('').to_dict('records'))
    except KeyError:
        return JSONResponse({'message': 'invalid key'}, HTTP_400_BAD_REQUEST)


if __name__ == "__main__":
    uvicorn.run('__main__:app', host=HOST, port=PORT)
